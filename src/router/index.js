import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import ContactForm from '@/views/ContactForm.vue'
import About from '@/views/About.vue'

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/form',
        name: 'ContactForm',
        component: ContactForm,
        meta: {
            reload: true
        }
    },
    {
        path: '/about',
        name: 'About',
        component: About
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
